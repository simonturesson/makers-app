//
//  ImageAnswerCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-11-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class ImageAnswerCell: UITableViewCell {

   
    
    @IBOutlet weak var answerImageView: UIImageView!
    var VC : QuestionViewController? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(ImageAnswerCell.changeImage))
        self.answerImageView.addGestureRecognizer(tap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeImage() -> Void {
        print("image tapped")
        if self.VC != nil {
            self.VC?.takeImage(imageView: answerImageView);
        }
    }

}
