//
//  Mission.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-20.
//  Copyright © 2016 persimon. All rights reserved.
//

import MapKit
import UIKit
import Firebase
import Foundation

struct missionModel {
    
    init(snap : FIRDataSnapshot, schoolId : String) {
        if let data : NSDictionary = snap.value as? NSDictionary {
            self.id = snap.key
            
            if data["title"] != nil  {
                self.title =  data["title"] as! String
            }
            if data["body"] != nil  {
                self.body =  data["body"] as! String
            }
            
            if data["showUserLocation"] != nil  {
                if data["showUserLocation"] is Bool {
                    self.showUserLocation =  data["showUserLocation"] as! Bool
                    print(self.showUserLocation)
                }
            }
            
            if data["mapType"] != nil  {
                self.mapType =  data["mapType"] as! String
            }
            
            self.schoolId = schoolId
        }
    }
    
    init() {
        
    }
    
    var id : String = ""
    var assignmentId = ""
    var title : String = ""
    var body : String = ""
    var schoolId : String = ""
    var showUserLocation : Bool = true
    var mapType : String = "Standard"
    var created : String = ""
    var updated : String = ""
    var questions: [questionModel] = []
    
    
    // MARK : Save
    func save(titleLabel : UITextField, descLabel : UITextField, question : questionModel, mapView : MKMapView, tableView : UITableView) {
        var ref : FIRDatabaseReference
        var type = "radio"
        var correctAnswers : Int = 0
        
        for item in question.answers {
            if item.isCorrect == true {
                correctAnswers = correctAnswers + 1
            }
        }
        
        if correctAnswers > 1 {
            type = "checkbox"
        } else {
            type = "radio"
        }
        
        // MARK : - Save mission
        let coordinates = mapView.annotations.last?.coordinate
        let blueprint : [String : Any] = [
            "title" : titleLabel.text!,
            "body" : descLabel.text!,
            "missionId" : self.id,
            "type" : type,
            "lat" : Double(coordinates!.latitude),
            "long" : Double(coordinates!.longitude)
        ]
        
        if question.id.characters.count > 0 {
            ref = FIRDatabase
                .database()
                .reference()
                .child("questions")
                .child(question.id)
            
        } else {
            ref = FIRDatabase
                .database()
                .reference()
                .child("questions")
                .childByAutoId()
        }
        
        
        // MARK : - Save question
        ref.setValue(blueprint)
        
        
        //MARK : - Save answer options
        for (index, answer) in question.answers.enumerated() {
            if answer.isDeleted {
                FIRDatabase.database().reference().child("questions").child(ref.key).child("answers").child(answer.id).removeValue()
            } else {
                let AORef = FIRDatabase.database().reference().child("questions").child(ref.key).child("answers")
                let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! editAnwerCell
                var isCorrect : Bool = false
                let title = cell.textEdit.text!
                
                if cell.accessoryType == .checkmark {
                    isCorrect = true
                } else {
                    isCorrect = false
                }
                
                let blueprint = [
                    "title" : title,
                    "isCorrect" : isCorrect
                    ] as [String : Any]
                
                if answer.id.characters.count == 0 {
                    AORef.childByAutoId().setValue(blueprint)
                } else {
                    AORef.child(answer.id).setValue(blueprint)
                }
            }
        }
    }
}

