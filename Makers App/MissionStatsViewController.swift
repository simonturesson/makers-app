//
//  MissionStatsViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-10-01.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import EasyAnimation

class MissionStatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var completed : completedModel = completedModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    var data :  [String : String] = ["Tid" : "", "Distans" : ""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = completed.title
        self.setGradient(view: self.view)
        self.setupStats()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear;
        tableView.alwaysBounceVertical = false;
        
        }
    
    func setupStats() {
        if self.completed.time.characters.count > 0 {
            self.data["Tid"] = self.completed.time
        }
        if self.completed.distance.characters.count > 0 {
            self.data["Distans"] = formatDistance(meters: Float(Double(self.completed.distance)!))
        }
        self.tableView.reloadData()
    }
    
    // MARK : - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StatsTableViewCell
        // MARK : - Data
        cell.backgroundColor = UIColor.clear;

        if indexPath.row == 0 {
            cell.keyLabel.text = "Tid förbrukad: "
            cell.valueLabel.text = self.data["Tid"]
        }
        
        if indexPath.row == 1 {
            cell.keyLabel.text = "Distans förflyttad: "
            cell.valueLabel.text = self.data["Distans"]
        }
        
        if indexPath.row == 2 {
            
        }
        
        
        // MARK : - Animation
        cell.alpha = 0
        UIView.animate(withDuration: (0.5), delay: Double(0.5 * Double(indexPath.row)), animations: {
            cell.alpha = 1
        })
        return cell
    }

}

/*
    Antal rätt av alla :
    Tid :
    Distance :
    Anvädare : 
 
 */
