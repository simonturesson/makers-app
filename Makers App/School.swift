//
//  School.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-27.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

struct schoolModel {
    
    init(snap : FIRDataSnapshot) {
        if let data : NSDictionary = snap.value as? NSDictionary {
            print("dic")
            if data["title"] != nil  {
                self.title =  data["title"] as! String
            }
        } else {
            self.title =  snap.value as! String
        }
        
        self.id = snap.key
        
        
    }
    
    init() {
        
    }
    
    var id : String = ""
    var title : String = ""
    var created : String = ""
    var updated : String = ""
    var missions : [missionModel] = []
}
