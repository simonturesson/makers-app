//
//  completedAnswers.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

class completedQuestionsModel {
    
    init(snap : FIRDataSnapshot) {
        let data : NSDictionary = snap.value as! NSDictionary
        self.id = snap.key
        
        if data["questionId"] != nil  {
            self.questionId =  data["questionId"] as! String
        }
        
    
        
    }
    
    init() {
        
    }
    
    func loadChildren() -> Void {
        if self.questionId.characters.count > 0 {
            let ref = FIRDatabase.database().reference().child("completed").child(self.completedId).child("questions").child(self.id).child("answers")
            ref.queryOrderedByKey().observe(.childAdded, with: {
                (snap : FIRDataSnapshot) in
                DispatchQueue.main.async {
                    self.answers.append(completedAnswersModel(snap: snap))
                }
            })
        }
    }

    var id : String = ""
    var questionId : String = ""
    var answerText : String = ""
    var answerImage : UIImage = UIImage()
    var answers : [completedAnswersModel] = []
    
    
    var completedId : String = ""
}
