//
//  DescCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-11-23.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class DescCell: UITableViewCell {

    @IBOutlet weak var descLabel: UILabel!

}
