//
//  EditViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-23.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import DZNEmptyDataSet
import SwiftLocation

class EditViewController: UIViewController, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    // MARK : - Outlets
    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var descLabel: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var cancelBtn: UITabBarItem!
    @IBOutlet weak var okBtn: UITabBarItem!
    
    // MARK : - Actions    
    @IBAction func didTouchAdd(_ sender: AnyObject) {
        self.tableView.beginUpdates()
        self.question.answers.append(answerModel())
        self.tableView.insertRows(at: [IndexPath(row: self.question.answers.count - 1, section: 0)], with: .automatic)
        self.tableView.endUpdates()
    }
    
    
    @IBAction func didTouchEdit(_ sender: AnyObject) {
        if self.tableView.isEditing {
            self.editButton.setTitle("Klar", for: UIControlState.normal)
        } else {
            self.editButton.setTitle("Redigera", for: UIControlState.normal)
        }
        
        self.tableView.setEditing(!self.tableView.isEditing, animated: true)
    }
    
    @IBAction func didTouchMapView(_ sender: UITapGestureRecognizer) {
        self.mapView.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.mapView.removeAnnotation($0)
            }
        }
        
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        self.addAnnotation(coordinate: coordinate)
    }
    
    var count : Int = 0
    var QARef : FIRDatabaseReference = FIRDatabase.database().reference()
    var mission : missionModel = missionModel()
    var question : questionModel = questionModel()
    
    // MARK : - Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setGradient(view: self.view)
        self.mapView.delegate = self
        self.mapView.setCenter(self.mapView.userLocation.coordinate, animated: true)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = Settings.sharedInstance.tableCellBottomBorderColor
        if self.question.id.characters.count > 0 {
            self.QARef = FIRDatabase
                .database()
                .reference()
                .child("questions")
                .child(self.question.id)
                .child("answers")
        
            self.QARef.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
                if snap.exists() {
                    self.question.answers.append(answerModel(snap: snap))
                    self.tableView.reloadData()
                }
            }
            
            self.QARef.queryOrderedByKey().observe(.childChanged) { (snap : FIRDataSnapshot) in
                if snap.exists() {
                    for index in 0...(self.question.answers.count - 1) {
                        if self.question.answers[index].id == snap.key {
                        self.question.answers[index] = answerModel(snap: snap)
                        self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        
        for item in self.tabBar.items! {
            item.titlePositionAdjustment = UIOffset(horizontal: 0.00, vertical: -16.00)
            item.setTitleTextAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16), NSForegroundColorAttributeName:UIColor.white], for: UIControlState.normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.question.id.characters.count > 0 {
            self.titleLabel.text = self.question.title
            self.descLabel.text = self.question.body
            
            let lat = self.question.lat
            let long = self.question.long
            
            self.addAnnotation(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long)))
        } else {
            self.titleLabel.text = ""
            self.descLabel.text = ""
        }
    }
    
    func goToLocation(_ location: CLLocation) {
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        self.mapView?.setRegion(region, animated: true)
    }
    
    // MARK : - Empty data set
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Det finns inga svarsalternativ till denna frågan!"
    
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline), NSForegroundColorAttributeName: UIColor.white]
        
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Tryck på plus-knappen för att lägga till svarsalernativ"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body), NSForegroundColorAttributeName: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    
    
    // MARK : - Table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.question.answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! editAnwerCell
        cell.backgroundColor = .clear
        cell.textEdit.text = self.question.answers[indexPath.row].title
        if self.question.answers[indexPath.row].isCorrect {
            cell.accessoryType = .checkmark
        }
        
        cell.separatorInset = .zero
        cell.layoutMargins = .zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Ta bort") { (UITableViewRowAction, indexPath) in
            if self.question.answers[indexPath.row].id.characters.count > 0 {
                self.question.answers[indexPath.row].isDeleted = true
            }
            self.question.answers.remove(at: indexPath.row )
            self.tableView.reloadData()
        }
        
        delete.backgroundColor = UIColor.red
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
        
        if selectedCell.accessoryType == UITableViewCellAccessoryType.checkmark {
           selectedCell.accessoryType = .none
        } else {
            selectedCell.accessoryType = .checkmark
        }
    }
    
    
    // MARK : - Map View
    func addAnnotation(coordinate : CLLocationCoordinate2D) -> Void {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        self.mapView.addAnnotation(annotation)
    }

}
