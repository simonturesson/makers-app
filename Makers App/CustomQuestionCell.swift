//  FieldsRow.swift
//  Eureka ( https://github.com/xmartlabs/Eureka )
//
//  Copyright (c) 2016 Xmartlabs SRL ( http://xmartlabs.com )
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Eureka
import Foundation

// Custom Cell with value type: Bool
// The cell is defined using a .xib, so we can set outlets :)
public class CustomQuestionCell: Cell<String>, CellType, UITextFieldDelegate {
    
    let screen = UIScreen.main.bounds
    var switchControl: UISwitch?
    var textField: UITextField?
    
    private var CustomQuestionRow : CustomQuestionRow {
        return row as! CustomQuestionRow
    }
    
    private var value : [String] {
        return (row.value?.components(separatedBy: "{}"))!
    }
    
    public override func setup() {
        super.setup()
        backgroundColor = .clear
        detailTextLabel?.alpha = 0
        detailTextLabel?.isHidden = true
        textField = UITextField(frame: CGRect(x: 8, y: 8, width: self.screen.width - 60, height: 28))
        textField?.delegate = self
        textField?.autocorrectionType = .default
        textField?.autocapitalizationType = .sentences
        textField?.keyboardType = .default
        textField?.text = value[0]
        textField?.attributedPlaceholder =  NSAttributedString(string: "Skriv här - Är detta svaret rätt? använd knappen till höger", attributes: [NSForegroundColorAttributeName: UIColor.white.withAlphaComponent(0.8)])
        textField?.textColor = .white
        //textField?.placeholder = CustomQuestionRow.placeholder
        textLabel?.textColor = .clear
        
        switchControl = UISwitch(frame: CGRect(x: self.screen.width - 68, y: 8, width: 40, height: 14))
        switchControl?.addTarget(self, action: #selector(CustomQuestionCell.switchValueChanged), for: .valueChanged)
        switchControl?.isOn = Bool(value[1])!
        contentView.backgroundColor = .clear
        contentView.addSubview(switchControl!)
        contentView.addSubview(textField!)
    }
    
    func switchValueChanged(){
        if let bool = switchControl?.isOn {
            row.value = (self.textField?.text!)! + "{}" + String(describing: bool) + "{}" + value[2]
            row.updateCell()
        }
    }
    
    public override func update() {
        super.update()
        backgroundColor = .clear
        super.textLabel!.alpha = 0
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let bool = switchControl?.isOn {
            row.value = (self.textField?.text!)! + "{}" + String(describing: bool) + "{}" + value[2]
            row.updateCell()
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let bool = switchControl?.isOn {
            row.value = (self.textField?.text!)! + "{}" + String(describing: bool) + "{}" + value[2]
            row.updateCell()
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let bool = switchControl?.isOn {
            row.value = (self.textField?.text!)! + "{}" + String(describing: bool) + "{}" + value[2]
            row.updateCell()
        }
        return true
    }
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class CustomQuestionRow: Row<CustomQuestionCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
    }
    
    
    public var setBackgroundColor : UIColor = .white
    public var isCorrect : Bool = false
    public var placeholder: String = ""
}
