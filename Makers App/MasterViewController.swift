//
//  MasterViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {
    
    // MARK : - Outlets
    @IBOutlet weak var label: UILabel!
    
    var completed : completedModel = completedModel()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setGradient(view: self.view)
        // Do any additional setup after loading the view.
        self.label.text = self.completed.title
    }


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
