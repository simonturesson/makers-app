//
//  Completed.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

class completedModel {
    init(snap : FIRDataSnapshot) {
        let data : NSDictionary = snap.value as! NSDictionary
        self.id = snap.key
        
        if data["missionId"] != nil  {
            self.missionId =  data["missionId"] as! String
        }
        
        if data["title"] != nil  {
            self.title =  data["title"] as! String
        }
        
        if data["isCompleted"] != nil  {
            self.isCompleted =  data["isCompleted"] as! Bool
        }
        if data["isRead"] != nil  {
            self.isRead = data["isRead"] as! Bool
        }
        if data["state"] != nil  {
            self.state = CompletedState(rawValue: data["state"] as! String)
        }
        
        if data["users"] != nil  {
            self.users = data["users"] as! [String]
        }
        
        if data["distance"] != nil  {
            self.distance =  data["distance"] as! String
        }
        if data["time"] != nil  {
            self.time =  data["time"] as! String
        }

        if data["created"] != nil  {
            self.created =  data["created"] as! String
        }
        if data["updated"] != nil  {
            self.updated =  data["updated"] as! String
        }
        
        self.loadChildren()
    }
    
    init() {
        
    }
    
    func loadChildren() -> Void {
        let ref = FIRDatabase.database().reference().child("completed").child(self.id).child("questions")
        ref.queryOrderedByKey().observe(.childAdded, with: {
            (snap : FIRDataSnapshot) in
            DispatchQueue.main.async {
                let model = completedQuestionsModel(snap: snap)
                model.completedId = self.id
                model.loadChildren()
                self.questions.append(model)
            }
        })
    }
    
    var id : String = ""
    var missionId : String = ""
    var title : String = ""
    var isCompleted : Bool = false
    var isRead : Bool = false
    var state : CompletedState = .unknown
    var users : [String] = []
    var distance : String = ""
    var time : String = ""
    var questions : [completedQuestionsModel] = []
    var updated : String = ""
    var created : String = ""
}
