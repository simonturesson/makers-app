//
//  StatsTableViewCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-10-01.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class StatsTableViewCell: UITableViewCell {
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}
