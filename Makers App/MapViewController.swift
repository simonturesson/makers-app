//
//  MapViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-16.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import Presentr
import CoreLocation
import EasyAnimation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapToolbar: UIView!
    @IBOutlet weak var sidebarView: UIView!
    @IBOutlet weak var trainlingContraint: NSLayoutConstraint!

    @IBAction func didTouchFindMe(_ sender: AnyObject) {
        goToLocation(self.lastLocation!)
    }
    @IBAction func didTouchMapType(_ sender: AnyObject) {
        let outlet = sender as! UISegmentedControl
        switch outlet.selectedSegmentIndex {
        case 0:
            self.mapView.mapType = .standard
        case 1:
            self.mapView.mapType = .satellite
        case 2:
            self.mapView.mapType = .hybrid
        default:
            self.mapView.mapType = .standard
        }
    }
    
    let screen = UIScreen.main.bounds
    var locationManager : CLLocationManager!
    var user : userModel?
    var mission = missionModel()
    let missionRef = FIRDatabase.database().reference().child("missions")
    var startLocation : CLLocation!
    var lastLocation : CLLocation? = nil
    var traveledDistance : Double = 0
    let regionRadius = 400
    var sidebarController : SidebarViewController?
    
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverVerticalFromTop
        return presenter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.mission.title
        self.setGradient(view: self.view)
        
        locationManager = CLLocationManager()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        }
        self.mapView.delegate = self
        self.mapView.showsUserLocation = self.mission.showUserLocation
        
        switch self.mission.mapType.lowercased() {
        case "satelit":
            self.mapView.mapType = .satellite
            break
        case "hybrid":
            self.mapView.mapType = .hybrid
            break
        default:
            self.mapView.mapType = .standard
            break
        }
        
        self.checkMapPositionPremission()
        self.startToCommunicateWithFireBase()
        self.locationManager.startUpdatingLocation()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        switch (self.mapView.mapType) {
        case .hybrid:
            self.mapView.mapType = .standard;
            break;
            
        case .standard:
            self.mapView.mapType = .hybrid;
            
            break
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        }
    }
    
    // MARK : - API
    func startToCommunicateWithFireBase() -> Void {
        // MARK - Items added
        let QARef = FIRDatabase.database().reference().child("questions")
        QARef.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            if snap.exists() {
                let question = questionModel(snap: snap)
                if  question.mission == self.mission.id {
                    if question.lat != Double(0) && question.long != Double(0) {
                        let location = CLLocationCoordinate2DMake(question.lat, question.long)
                        let annotation = MKPointAnnotation()
                        
                        annotation.coordinate = location;
                        annotation.title = question.title;
                        
                        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                            let circle = MKCircle(center: location, radius: CLLocationDistance(self.regionRadius))
                            self.mapView.add(circle)
                        } else {
                            question.isLocked = false
                        }
                        
                        self.mapView.addAnnotation(annotation)
                    } else {
                        question.isLocked = false
                    }
                    
                    
                    if (self.sidebarController != nil) {
                        print(question)
                        self.sidebarController?.addQuestion(question: question)
                    }
                    self.mission.questions.append(question)
                }
            }
        }
        
        QARef.queryOrderedByKey().observe(.childChanged) { (snap : FIRDataSnapshot) in
            if snap.exists() {
                let question = questionModel(snap: snap)
                if  question.mission == self.mission.id {
                    if question.lat != Double(0) && question.long != Double(0) {
                        for count in 0...self.mission.questions.count - 1 {
                            if self.mission.questions[count].id == question.id {
                                
                                for anno in self.mapView.annotations {
                                    let old : CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.mission.questions[count].lat, self.mission.questions[count].long)
                                    if Double(anno.coordinate.latitude) == Double(old.latitude) && Double(anno.coordinate.longitude) == Double(old.longitude)  {
                                        self.mapView.removeAnnotation(anno)
                                        let annotation = MKPointAnnotation()
                                        let location : CLLocationCoordinate2D = CLLocationCoordinate2DMake(question.lat, question.long)
                                        annotation.coordinate = location;
                                        annotation.title = question.title;
                                        
                                        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                                            let circle = MKCircle(center: location, radius: CLLocationDistance(self.regionRadius))
                                            self.mapView.add(circle)
                                        } else {
                                            question.isLocked = self.mission.questions[count].isLocked
                                        }
                                        
                                        self.mapView.addAnnotation(annotation)
                                    }
                                }
                                self.mission.questions[count] = question
                            }
                        }
                    }
                }
            }
        }
        
        
    }

    
    
    // MARK : - Map View
    func checkMapPositionPremission() -> Void {
        // 1. status is not determined
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
            // 2. authorization were denied
        else if CLLocationManager.authorizationStatus() == .denied {
            let title = "Saknar tillåtelse"
            let body = "Du har tidigare nekat applikationen tillgång till din position. För att kunna använda tjänsten måste du tillåta detta i applikationens inställningar"
            
            let controller = Presentr.alertViewController(title: title, body: body)
            let okAction = AlertAction(title: "Ok", style: .cancel) {
                self.dismiss(animated: true, completion: nil)
            }
            
            
            controller.addAction(okAction)
            
            presenter.presentationType = .alert
            customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        }
            // 3. we do have authorization
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = Settings.sharedInstance.mainBgColorFrom
        circleRenderer.fillColor =  Settings.sharedInstance.mainBgColorFrom.withAlphaComponent(0.1)
        circleRenderer.lineWidth = 1.0
        return circleRenderer
    }
    
    
    // MARK: - Location
    
    func resetTraveledDistance() {
        self.traveledDistance = 0.0
    }
    
    func goToLocation(_ location: CLLocation) {
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
        } else {
            if let lastLocation = locations.last {
                let lastDistance = self.lastLocation?.distance(from: lastLocation)
                self.traveledDistance += lastDistance!
                /*
                print( "\(startLocation)")
                print( "\(lastLocation)")
                print("FULL DISTANCE: \(self.traveledDistance)")
 */
            }
        }
        self.lastLocation = locations.last
        locationManager.startMonitoringVisits()
        for annotation in self.mapView.annotations {
            if annotation is MKUserLocation {} else {
                /*
                let a = MKMapPoint(x: (locations.last?.coordinate.latitude)!, y: (locations.last?.coordinate.longitude)!)
                let b = MKMapPoint(x: annotation.coordinate.latitude, y: annotation.coordinate.longitude)
                let meters = MKMetersBetweenMapPoints(a, b)
                print(meters)
                */

                let dis = locations.last?.distance(from: CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude))
                if Int(dis as Double!) < self.regionRadius {
                    if self.sidebarController != nil {
                        for question in self.mission.questions {
                            if question.title == annotation.title! && question.isLocked {
                                self.sidebarController?.userDidEnterQuestionsRegion(questionId: question.id)
                            }
                        }
                    }
                    print("in position")
                }
            }
            
        }
        
    }
    
    
    
    // MARK : - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sidebarSegue" {
            let VC = segue.destination as! SidebarViewController
            self.sidebarController = VC
            VC.user = self.user!
            VC.mission = self.mission
            VC.mapView = self.mapView
            VC.parentController = self
        }
    }
    
}
