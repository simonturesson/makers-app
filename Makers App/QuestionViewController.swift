
//
//  QuestionViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-22.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import Presentr
import M13Checkbox
import DZNEmptyDataSet
import MXParallaxHeader

class QuestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UITabBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var clearBtn: UITabBarItem!
    @IBOutlet weak var OkBtn: UITabBarItem!
    
    @IBAction func didTouchShowOnMap(_ sender: AnyObject) {
        goToLocation(CLLocation(latitude: CLLocationDegrees(self.question.lat), longitude: CLLocationDegrees(self.question.long)))
    }
    
    var mission : missionModel = missionModel()
    var question : questionModel = questionModel()
    var completed : completedModel?
    var checkboxes : [M13Checkbox] = []
    var mapView : MKMapView?
    var parentVC : SidebarViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setGradient(view: self.view)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView()
        self.tabBar.delegate = self
        
        if self.question.image != "" {
            self.question.getImage(completion: { (image : UIImage) in
                let headerView = UIImageView(image: image)
                headerView.contentMode = .scaleAspectFill;
                
                self.tableView.parallaxHeader.view = headerView;
                self.tableView.parallaxHeader.height = 250;
                self.tableView.parallaxHeader.mode = MXParallaxHeaderMode.fill;
                self.tableView.parallaxHeader.minimumHeight = 20;
            })
        
        
        }
        
        for item in self.tabBar.items! {
            item.titlePositionAdjustment = UIOffset(horizontal: 0.00, vertical: -16.00)
            item.setTitleTextAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16), NSForegroundColorAttributeName:UIColor.white], for: UIControlState.normal)
        }
        
        let ref = FIRDatabase
            .database()
            .reference()
            .child("questions")
            .child(self.question.id)
            .child("answers")
        
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            if !((self.question.answers.index(where: { (answer : answerModel) -> Bool in
                if answer.id == snap.key {
                    return true
                }
                
                return false
            }) != nil)) {
                self.question.answers.append(answerModel(snap: snap))
            }
        
            self.tableView.reloadData()
        }
        
        ref.queryOrderedByKey().observe(.childChanged) { (snap : FIRDataSnapshot) in
            for (index, el) in self.question.answers.enumerated() {
                if el.id == snap.key {
                   self.question.answers[index] = answerModel(snap: snap)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (self.parentVC !== nil) {
            let parent = self.parentVC
            let index =  IndexInArray(id: self.question.id, arr: (parent?.completed.questions)!)
            let model = completedQuestionsModel()
            
            /* Remove all */
            if (parent?.completed.questions.count)! > 0 {
                for index in 0...((parent?.completed.questions.count)! - 1) {
                    if parent?.completed.questions[index].questionId == self.question.id {
                        parent?.completed.questions[index].answers.removeAll()
                    }
                }
            }
            
            model.questionId = self.question.id
            
            if self.question.type == .choose {
                for (index, checkbox) in self.checkboxes.enumerated() {
                    if checkbox.checkState == .checked {
                        var answer = completedAnswersModel()
                        if self.question.answers[index].isCorrect {
                            answer.isCorrect = true
                        }
                        answer.answers.append(self.question.answers[index].id)
                        model.answers.append(answer)
                    } else {
                    }
                }
            } else if self.question.type == .media {
                let cell = self.tableView.visibleCells[2] as! ImageAnswerCell
                if (cell.answerImageView.image != nil) {
                    model.answerImage = cell.answerImageView.image!
                }
                
            } else if self.question.type == .text {
                let cell = self.tableView.visibleCells[2] as! TextAnswerCell
                if (cell.textArea.text != nil) {
                 model.answerText = cell.textArea.text
                }
            }
        
            
            if index != -1 {
                self.completed?.questions[index] = model
                parent?.completed.questions[index] = model
            } else {
                parent?.completed.questions.append(model)
            }
        }
    }
    
    func IndexInArray(id: String, arr : [completedQuestionsModel]) -> Int {
        for (index, question) in arr.enumerated() {
            if question.id == id {
                return index
            }
        }
        return -1
    }
    
    func goToLocation(_ location: CLLocation) {
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        self.mapView?.setRegion(region, animated: true)
    }
    
    // MARK : - Tabbar
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title  == "Rensa" {
            for box  in self.checkboxes {
                box.setCheckState(.unchecked, animated: true)
            }
            
            let cells = tableView.visibleCells
            for cell in cells {
                if cell is answerCell {
                    cell.contentView.backgroundColor = UIColor.clear
                }
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK : - Empty data set
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Det finns inga svarsalternativ till denna frågan"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline), NSForegroundColorAttributeName: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    



    
    // MARK: - Table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.question.type == .choose {
            return self.question.answers.count + 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as! TitleCell
            cell.title.text = self.question.title
            cell.backgroundColor = .clear
            return cell
            
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "desc", for: indexPath) as! DescCell
            cell.backgroundColor = .clear
            if self.question.body.characters.count > 0 {
                    cell.descLabel.text = self.question.body
            } else {
                cell.descLabel.text = ""
            }
            return cell
            
        }
 
        if self.question.type == .choose {
            let cell = tableView.dequeueReusableCell(withIdentifier: "answer", for: indexPath) as! answerCell
            cell.checkbox.stateChangeAnimation = .expand(.fill)
            self.checkboxes.append(cell.checkbox)
            cell.backgroundColor = UIColor(white: 1, alpha: 0.1)
            cell.tintColor = UIColor.white
            cell.label.text = self.question.answers[indexPath.row - 2].title
            cell.separatorInset = .zero
            cell.layoutMargins = .zero
            
            // MARK : - Set previus answer
            if self.completed!.questions.count > 0 {
                for index in 0...((self.completed?.questions.count)! - 1) {
                    let question = self.completed?.questions[index]
                    if question?.questionId == self.question.id {
                        for answer in (question?.answers)! {
                            for opt in answer.answers {
                                if opt == self.question.answers[indexPath.row - 2].id {
                                    cell.checkbox.setCheckState(.checked, animated: true)
                                    self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
                                    
                                    // MARK : - Animate change
                                    UIView.animate(withDuration: 0.4, animations: {
                                        cell.contentView.backgroundColor = Settings.sharedInstance.selectedQuestion
                                    })
                                }
                            }
                        }
                    }
                }
            }
            return cell
            
            // Media answer
        } else if self.question.type == .media {
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! ImageAnswerCell
            cell.backgroundColor = .clear
            cell.VC = self
            if self.completed!.questions.count > 0 {
                for index in 0...((self.completed?.questions.count)! - 1) {
                    let question = self.completed?.questions[index]
                    if question?.questionId == self.question.id {
                        cell.answerImageView.image = question?.answerImage
                    }
                }
            }
            return cell
        }
        
        // Defualt - Text area
        let cell = tableView.dequeueReusableCell(withIdentifier: "text", for: indexPath) as! TextAnswerCell
        cell.backgroundColor = .clear
        if self.completed!.questions.count > 0 {
            for index in 0...((self.completed?.questions.count)! - 1) {
                let question = self.completed?.questions[index]
                if question?.questionId == self.question.id {
                    cell.textArea.text = question?.answerText
                    cell.updateStyle()
                }
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.question.type == .choose {
            if indexPath.row > 1 {
                let selectedCell : answerCell = tableView.cellForRow(at: indexPath)! as! answerCell
                
                if self.question.chooseType == .radio {
                    // MARK : - Radio button
                    for checkbox in self.checkboxes {
                        if checkboxes.index(of: checkbox) == indexPath.row - 2 {
                            checkbox.setCheckState(M13Checkbox.CheckState.checked, animated: true)
                        } else {
                            checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
                        }
                    }
                } else {
                    
                    // MARK : - Checkbox
                    let checkbox = checkboxes[indexPath.row - 2]
                    if checkbox.checkState == .checked {
                        checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: true)
                    } else {
                        checkbox.setCheckState(M13Checkbox.CheckState.checked, animated: true)
                        self.tableView.deselectRow(at: indexPath, animated: true)
                    }
                }
                
                
                // MARK : - Animate change
                UIView.animate(withDuration: 0.4, animations: {
                    if selectedCell.checkbox.checkState == .checked {
                        selectedCell.contentView.backgroundColor = Settings.sharedInstance.selectedQuestion
                    } else {
                        selectedCell.contentView.backgroundColor = UIColor.clear
                    }
                })
            }
        }

    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if self.question.type == .choose {
            if indexPath.row > 1 {
                let selectedCell : answerCell = tableView.cellForRow(at: indexPath) as! answerCell
                
                // MARK : - Animate change
                UIView.animate(withDuration: 0.1, animations: {
                    if selectedCell.checkbox.checkState == .checked {
                        selectedCell.contentView.backgroundColor = Settings.sharedInstance.selectedQuestion
                    } else {
                        selectedCell.contentView.backgroundColor = UIColor.clear
                    }
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.question.type != .choose && indexPath.row == 2 {
            return CGFloat(300)
        }
        
        if indexPath.row == 1 && self.question.body == "" {
                return CGFloat(0)
        }
        return CGFloat(60)
    }
    
    
    // Take Image
    var imageView : UIImageView? = nil
    func takeImage(imageView : UIImageView) {
        self.imageView = imageView
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            print("presenting")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("did")
        if (self.imageView != nil) && (info[UIImagePickerControllerOriginalImage] != nil) {
            self.imageView?.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        }
        self.dismiss(animated: true, completion: nil)
    }

}

/*
 completedQuestionsModel(id: "", questionId: "-KSffLGET5VOhwD6mL-A", answers: [Makers_App.completedAnswersModel(id: "", isCorrect: false, photoUrl: "", body: "", answers: ["-KSffLGI3GL3-q6i-PTM"]), Makers_App.completedAnswersModel(id: "", isCorrect: false, photoUrl: "", body: "", answers: ["-KTVaYrBmOx2kW7A5zpM"])])
 */
