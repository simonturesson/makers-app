//
//  questionCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-22.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class questionCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
}
