//
//  DistanceHelper.Swift
//  Wildcard
//
//  Created by Samuel Beek on 01-05-15.
//  Copyright (c) 2015 Wildcard. All rights reserved.
//

import Foundation

extension String {
    
}

func formatDistance(meters: Float) -> String {
    
    
    if(meters > 900) {
        let distance = String(format:"%.1f", (meters/1000))
        return "\(distance)km"
        
    } else {
        return "\(String(format:"%.1f", (meters)))m"
    }
}
