//
//  NavigationViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-22.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import DynamicButton

class NavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarStyle(.lightContent)
        let img = UIImage()
        automaticallyAdjustsScrollViewInsets = true
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = img
        self.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        self.navigationBar.barTintColor = UIColor(red:0.22, green:0.27, blue:0.31, alpha:1.00).flatten()
        self.navigationBar.tintColor = UIColor.white
        self.setGradient(view: self.view)
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }

}
