//
//  SidebarView.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-20.
//  Copyright © 2016 persimon. All rights reserved.
//
import UIKit
import Firebase
import MapKit
//import DateTools
import PopupDialog
import Presentr
import CZPicker
import Spring
import DynamicButton
import EasyAnimation
import DZNEmptyDataSet
import UIImageView_Letters
import TransitionTreasury
import TransitionAnimation

protocol questionStatusDelegate {
    func userDidEnterQuestionsRegion(questionId: String)
}

class SidebarViewController: UIViewController, MenuTransitionManagerDelegate, ModalTransitionDelegate, CZPickerViewDelegate, CZPickerViewDataSource {
    
    internal func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - Setup
    var parentController : MapViewController? = nil
    let ref = FIRDatabase.database().reference()
    var edit : UIBarButtonItem?
    var user : userModel = userModel()
    var appendNewButton : SpringImageView? = nil
    var mission = missionModel()
    var question = questionModel()
    var completed = completedModel()
    var mapView : MKMapView?
    let screenSize: CGRect = UIScreen.main.bounds
    var row : String = ""
    let startTime : NSDate = NSDate()
    var imageArray : [SpringImageView] = []
    let color = Settings.sharedInstance.mainBgColorFrom.flatten()
    let menuTransitionManager = MenuTransitionManager()
    var picker : CZPickerView?
    var users : [userModel] = []
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = .coverVerticalFromTop // Optional
        return presenter
    }()
    
     var tr_presentTransition: TRViewControllerTransitionDelegate?
    
    // MARK: - Outlet
    @IBOutlet weak var appendButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Action
    @IBAction func didTouchCancel(_ sender: AnyObject) {
        self.dismiss(animated: true)
    }
    

    @IBAction func didTouchAppend(_ sender: AnyObject) {
        self.editQuestion(question: questionModel())
        
    }
    
    @IBAction func didTouchSend(_ sender: AnyObject) {
        if self.completed.questions.count > 0 {
            let ref = self.ref.child("completed").childByAutoId()
            //let nav = self.parent as! NavigationViewController
            let mapController = self.parent as! MapViewController
            //self.startTime.shortTimeAgoSinceNow()
            var model : [String : Any] = [
                "title" : self.mission.title,
                "missionId" : self.completed.missionId,
                "time" : "",
                "distance" : String(mapController.traveledDistance),
                "isCompleted" : true,
                "isRead" : false,
                "state" : "unknown",
                "users" : self.completed.users,
                "created" : String(describing: NSDate())
            ]
            ref.setValue(model)
            for (index, question) in self.completed.questions.enumerated() {
                let QARef = FIRDatabase.database().reference().child("completed").child(ref.key).child("questions").childByAutoId()
                model = [
                    "questionId" : question.questionId
                ]
                
                QARef.setValue(model)
                
                // MARK : - Save anwsers
                for answer in self.completed.questions[index].answers {
                    let Aref = FIRDatabase.database().reference().child("completed").child(ref.key).child("questions").child(QARef.key).child("answers").childByAutoId()
                    if answer.answers.count > 0 {
                        Aref.setValue([
                            "answerId" : answer.answers,
                            "isCorrect" : answer.isCorrect
                            ])
                    } else {
                        Aref.setValue([
                            "body" : answer.body,
                            "photoUrl" : answer.photoUrl,
                            "isCorrect" : answer.isCorrect
                            ])
                    }
                }
            }
            
            if self.mission.assignmentId != "" && self.mission.assignmentId.characters.count > 0 {
                FIRDatabase.database().reference().child("assignments").child(self.mission.assignmentId).child("isCompleted").setValue(true)
            }
            mapController.resetTraveledDistance()
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    var selectedQuestion : questionModel = questionModel()
    func questionTapped(sender : AnyObject) -> Void {
        let id = sender.view.accessibilityHint!
        for question : questionModel in self.mission.questions {
            if question.id == id {
                if self.isEditingQuestions {
                    self.editQuestion(question: question)
                } else {
                    if !question.isLocked || question.lat == 0 && question.long == 0 {
                        print("show in popup")
                        self.questionPopup(mission: self.mission, question: question, saveButtonTitle: "")
                    } else {
                        print("show on map")
                        let mapController = self.parent as! MapViewController
                        mapController.goToLocation(CLLocation(latitude: CLLocationDegrees(question.lat), longitude: CLLocationDegrees(question.long)))

                    }
                }
            }
        }
    }
    
    // MARK : - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.alpha = 0
        self.setupButton()
        
        // MARK : - Picker
        self.picker = CZPickerView(headerTitle: "Välj klasskamrater", cancelButtonTitle: "Avbryt", confirmButtonTitle: "Spara")
        self.picker?.delegate = self
        self.picker?.dataSource = self
        self.picker?.headerBackgroundColor = Settings.sharedInstance.mainBgColorTo
        self.picker?.cancelButtonNormalColor = UIColor.gray.flatten()
        self.picker?.confirmButtonNormalColor = UIColor.white
        self.picker?.checkmarkColor = Settings.sharedInstance.mainBgColorTo
        self.picker?.headerTitleColor = UIColor.white
        self.picker?.allowMultipleSelection = true
        self.getUsers()

        
        
        self.completed.missionId = self.mission.id
        self.returnUserRef { (user : userModel) in
            self.user = user
            if self.user.type != .admin {
                self.appendButton.isEnabled = false
                self.navigationItem.rightBarButtonItem = nil
            }
            self.completed.users.append(user.id)
        }
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.edit?.image = UIImage.fontAwesomeIcon(.edit, textColor: UIColor.white, size: CGSize(width: 30, height: 30))
        self.scrollView.alpha = 1
        self.findQuestions()
        
        if self.appendNewButton == nil {
            print("appending new")
            self.appendNewButton = SpringImageView(frame: CGRect(x: 20, y: 0, width: 60, height: 60))
            let tapGesture = UITapGestureRecognizer(target: self, action:#selector(SidebarViewController.didTouchAppend(_:)))
            
            self.appendNewButton?.autohide = true
            self.appendNewButton?.animation = "slideUp"
            self.appendNewButton?.setImageWith("+", color: self.color?.withAlphaComponent(0.6), circular: true)
            self.appendNewButton?.isUserInteractionEnabled = true
            self.appendNewButton?.addGestureRecognizer(tapGesture)
            //self.scrollView.contentSize.width = self.scrollView.contentSize.width * CGFloat(0 + 1)
            self.scrollView.addSubview(self.appendNewButton!)
            self.imageArray.insert(self.appendNewButton!, at: 0)
        }
    }
    
    func setupButton() -> Void {
            if (self.parentController != nil) {
                if self.user.type == .admin {
                    self.edit = UIBarButtonItem(image: UIImage.fontAwesomeIcon(.edit, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(SidebarViewController.editView(_:)))
                    self.parentController?.navigationItem.rightBarButtonItem = edit
                }
                
                self.parentController?.navigationItem.rightBarButtonItems?.insert(UIBarButtonItem(image: UIImage.fontAwesomeIcon(.users, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(SidebarViewController.addTeam(_:))), at: 1)
            }

        let complete = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        complete.layer.cornerRadius = 15
        complete.tintColor = .white
        complete.setTitle("Lämna in", for: UIControlState.normal)
        complete.backgroundColor = UIColor.green.flatten()
        complete.addTarget(self, action: #selector(SidebarViewController.didTouchSend(_:)), for: UIControlEvents.touchUpInside)
        
        if (self.parentController != nil) {
            self.parentController?.navigationItem.rightBarButtonItems?.insert(UIBarButtonItem(customView: complete), at: 0)
        }
        
    }
    
    func addTeam(_ sender : UIBarButtonItem) {
        self.picker?.show()
    }
    
    func userDidEnterQuestionsRegion(questionId: String) {
        print(questionId + " is in position")
        let index = self.mission.questions
        for index in 0...(self.mission.questions.count - 1)  {
            let quest = self.mission.questions[index]
            if quest.id == questionId {
                self.mission.questions[index].isLocked = false
                for i in 0...(self.scrollView.subviews.count - 1) {
                    if self.scrollView.subviews[i].accessibilityHint == quest.id {
                        let view = self.scrollView.subviews[i] as! SpringImageView
                        
                        let when = DispatchTime.now() + 1
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            print("setting")
                            view.setImageWith(String(index + 1), color: self.color, circular: true)
                            //view.animation = "pop"
                            //view.animate()
                        }
                    }
                }
                
            }
        }
    }
    
    func addQuestion(question : questionModel) {
        var offset = 0
        if self.user.type == .admin {
            offset = 1
            }
        
        

        let index = self.mission.questions.count
        let image = SpringImageView(frame: CGRect(x: 72 * (index + offset) + 20, y: 0, width: 60, height: 60))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SidebarViewController.questionTapped(sender:)))
        
        if question.isLocked {
            image.setImageWith(String.fontAwesomeIcon(.lock), color: self.color, circular: true, textAttributes: [ NSFontAttributeName: UIFont.fontAwesome(ofSize: 30), NSForegroundColorAttributeName: UIColor.white ])
        } else {
            image.setImageWith(String(index + 1), color: self.color, circular: true)
        }
        /*
         image.autohide = true
         image.animation = "slideUp"
         */
        image.accessibilityHint = question.id
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(tapGesture)
        //self.scrollView.contentSize.width = self.scrollView.contentSize.width * CGFloat(index + 1 + offset)
        self.scrollView.addSubview(image)
        self.imageArray.append(image)
        
        let width = 80 * (index + 1)
        let leftInset : CGFloat = CGFloat(Int(self.view.frame.width) / 2 - width / 2)
        UIView.animate(withDuration: 0.1, delay: 0.1 * Double(index), animations: {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: 0)
        }, completion: { (finnish : Bool) in
            //image.animate()
        })
        self.mission.questions.append(question)
    }
    
    
    // MARK : - Firebase
    func findQuestions() -> Void {
        self.ref.child("questions").queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            if snap.exists() {
                let data = snap.value as! NSDictionary
                let id = data["missionId"] as! String
                if  id == self.mission.id {
                    if !self.mission.questions.contains(where: { $0.id == snap.key }) {
                        
                        
                    }
                    
                }
            }
        }
        
        self.ref.child("questions").queryOrderedByKey().observe(.childChanged) { (snap : FIRDataSnapshot) in
            if snap.exists() {
                let data = snap.value as! NSDictionary
                let id = data["missionId"] as! String
                if  id == self.mission.id {
                    for index in 0...(self.mission.questions.count - 1) {
                        if snap.key == self.mission.questions[index].id {
                            self.mission.questions[index] = questionModel(snap: snap)
                     
                        }
                    }
                }
            }
        }
    }
    
    func findCompleted() -> Void {
        self.ref.child("completed")
            .queryOrderedByKey()
            .observe(.childAdded) { (snap : FIRDataSnapshot) in
            if snap.hasChildren() {
                let data = snap.value as! NSDictionary
                if let id = data["missionId"] {
                    if  id as! String == self.mission.id {
                       print(snap)
                    }
                }
            } else {
                self.ref.child("completed").childByAutoId().setValue(["missionId" : self.mission.id])
            }
        }
    }
    
    
    func goToLocation(_ location: CLLocation) {
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        self.mapView?.setRegion(region, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    var isEditingQuestions: Bool = false
    func editView(_ sender : UIBarButtonItem) {
        self.isEditingQuestions = !self.isEditingQuestions
        
            for i in 0...(self.imageArray.count - 1) {
                let view = self.imageArray[i]
                if self.isEditingQuestions {
                    if (view.accessibilityHint != nil) {
                        sender.image = UIImage.fontAwesomeIcon(.times, textColor: UIColor.white, size: CGSize(width: 30, height: 30))
                        view.animation = "pop"
                        view.duration = 1
                        view.delay = 1
                        view.repeatCount = 999
                        view.animate()
                    }
                } else {
                    view.repeatCount = 0
                    view.layer.removeAllAnimations()
                    sender.image = UIImage.fontAwesomeIcon(.edit, textColor: UIColor.white, size: CGSize(width: 30, height: 30))
                    
                }
            }
    }
    
    func editPopup(mission : missionModel, question : questionModel, saveButtonTitle : String) -> Void {
        let viewControllerStoryboardId = "EditMissionViewController"
        let storyboardName = "EditMission"
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerStoryboardId) as! EditViewController
        controller.mission = mission
        controller.question = question
        presenter.presentationType = .popup
        customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        

        //self.mission.save(titleLabel: viewController.titleLabel, descLabel: viewController.descLabel, question: viewController.question, mapView: viewController.mapView, tableView: viewController.tableView)
    }
    
    func editQuestion(question : questionModel) {
        self.isEditingQuestions = false
        let viewControllerStoryboardId = "EditQuestion"
        let storyboardName = "Mission"
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerStoryboardId) as! EditQuestionViewController
        controller.mission = self.mission
        controller.question = question
        controller.modalDelegate = self // Don't forget to set modalDelegate
        for annotation in (self.mapView?.annotations)! {
            if annotation is MKUserLocation {
                controller.position = annotation.coordinate
            }
        }
        
        tr_presentViewController(controller, method: TRPresentTransitionMethod.twitter, completion: {
            print("Present finished.")
        })
    }
    
    func questionPopup(mission : missionModel, question : questionModel, saveButtonTitle : String) -> Void {
        if question.isLocked == false {
            let width = ModalSize.custom(size: 500)
            let height = ModalSize.custom(size: 500)
            let center = ModalCenterPosition.center
            let viewControllerStoryboardId = "QuestionViewController"
            let storyboardName = "Mission"
            let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerStoryboardId) as! QuestionViewController
            controller.mission = mission
            controller.question = question
            controller.mapView = self.mapView!
            controller.completed = self.completed
            controller.parentVC = self
            presenter.presentationType = .custom(width: width   , height: height, center: center)
            customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        }
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //self.mapView?.isUserInteractionEnabled = false
        //self.mapView?.showsUserLocation = false
    }
    
    // MARK : - CZPickerView
    func getUsers() -> Void {
        let ref = FIRDatabase.database().reference().child("users")
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            let model = userModel(snap: snap)
            if model.id != self.user.id {
                if model.schoolId == self.user.schoolId {
                    self.users.append(model)
                    self.picker?.reloadData()
                }
            }
        }
        
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [Any]!) {
        for row in rows as! [Int] {
            self.completed.users.append(self.users[row].id)
        }
        pickerView.reloadData()
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return self.users[row].firstName.capitalizingFirstLetter() + " " + self.users[row].lastName.capitalizingFirstLetter()
    }
    
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return self.users.count
    }
}

extension UILabel{
    
    func requiredHeight() -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
}
