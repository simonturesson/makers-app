//
//  EditQuestionViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-11-09.
//  Copyright © 2016 persimon. All rights reserved.
//


import UIKit
import Eureka
import MapKit
import ImageRow
import Firebase
import MXParallaxHeader
import TransitionTreasury


class EditQuestionViewController: FormViewController, MKMapViewDelegate {
    
    let screen : CGRect = UIScreen.main.bounds
    var position : CLLocationCoordinate2D?
    var mission = missionModel()
    var question = questionModel()
    var switches : [UISwitch] = []
    var mapTap : UITapGestureRecognizer?
    let mapView : MKMapView = {
        let view = MKMapView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 284))
        return view
    }()
    var type : String = "Välj"
    
    weak var modalDelegate: ModalViewControllerDelegate?
    
    func dismiss() {
        modalDelegate?.modalViewControllerDismiss(callbackData: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func didTouchCancel(sender : AnyObject) -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTouchSave(sender : AnyObject) -> Void {
        let ref = FIRDatabase.database().reference().child("questions")
        var QRef : FIRDatabaseReference?
        if self.question.id.characters.count > 0 {
            QRef = ref.child(self.question.id)
        } else {
            QRef = ref.childByAutoId()
        }
        
        var data : [String : Any] = ["missionId": self.mission.id, "chooseType" : "radio"]
        var answers : [answerModel] = []
        var correntAnswers : Int = 0
        let values = self.form.values()
        print(self.type)
        switch self.type {
        case "Media":
            data["type"] = "media"
            break
        case "Välj":
            data["type"] = "choose"
            break
        default:
            data["type"] = "text"
            break
        }
        

        
        values.forEach{(key: String, value: Any?) in
            
            if key == "title" {
               return data["title"] = value as! String
            }
            
            if key == "desc" {
               return data["body"] = value as! String
            }
            
            if key == "image" {
                if value != nil {
                    let storageRef = FIRStorage.storage().reference()
                    let data : Data = UIImageJPEGRepresentation(value as! UIImage, 0.8)!
                    let metaData = FIRStorageMetadata()
                    metaData.contentType = "image/jpg"
                    storageRef.child("images").child(self.mission.id).child(self.randomString(length: 6)).put(data, metadata: metaData){(metaData,error) in
                        if let error = error {
                            print(error.localizedDescription)
                            return
                        } else {
                            let downloadURL = metaData!.downloadURL()!.absoluteString
                            QRef?.child("image").setValue(downloadURL)
                        }
                    }
                }
            }
            
            // Fetch answers from UI
            if data["type"] as! String == "choose" {
                print("finding questions")
                let answer = key.components(separatedBy: "answer-")
                if answer.count > 1 && (Int(answer[1]) != nil) {
                    let parse = (value as! String).components(separatedBy: "{}")
                    print(parse)
                    var model = answerModel()
                    model.id = parse[2]
                    model.title = parse[0]
                    model.isCorrect = Bool(parse[1])!
                    
                    // Check how many if correct
                    if model.isCorrect {
                        correntAnswers = correntAnswers + 1
                    }
                    answers.append(model)
                }
            }
            
           
        }
        

        // Get Selected location from question
        self.mapView.annotations.forEach {
            if !($0 is MKUserLocation) {
                print($0.coordinate)
                data["lat"] = $0.coordinate.latitude
                data["long"] = $0.coordinate.longitude
            }
        }
        if data["type"] as! String == "choose" {
            if correntAnswers > 1 {
                data["chooseType"] = "checkbox"
            } else {
                data["chooseType"] = "radio"
            }
        }
        
        // Save Question
        QRef?.setValue(data)
        
        // Save Answers
        if data["type"] as! String == "choose" {
            print("finding missions")
            answers.forEach { (answer : answerModel) in
                var ref : FIRDatabaseReference?
                if answer.id.characters.count > 0 {
                    ref = QRef?.child("answers").child(answer.id)
                } else {
                    ref = QRef?.child("answers").childByAutoId()
                }
                
                let data : [String : Any] = ["title": answer.title, "isCorrect" : answer.isCorrect]
                ref?.setValue(data)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTouchMapView(_ sender: UITapGestureRecognizer) {
        self.mapView.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.mapView.removeAnnotation($0)
            }
        }
        
        let location = sender.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        self.addAnnotation(coordinate: coordinate)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let close = UIButton(frame: CGRect(x: 16, y: 36, width: 100, height: 32))
        close.addTarget(self, action: #selector(EditMissionViewController.didTouchCancel(sender:)), for: UIControlEvents.touchUpInside)
        close.setTitle("Avbryt", for: UIControlState.normal)
        close.setTitleColor(UIColor.purple.flatten(), for: UIControlState.normal)
        close.sizeToFit()
        
        let save = UIButton(frame: CGRect(x: self.screen.width - 116, y: 36, width: 100, height: 32))
        save.addTarget(self, action: #selector(EditMissionViewController.didTouchSave(sender:)), for: UIControlEvents.touchUpInside)
        save.setTitle("Spara", for: UIControlState.normal)
        save.backgroundColor = UIColor.green.flatten()
        save.layer.cornerRadius = 16
        
        view.addSubview(close)
        view.addSubview(save)

        
        self.mapTap = UITapGestureRecognizer(target: self, action: #selector(EditQuestionViewController.didTouchMapView(_:)))
        self.mapView.addGestureRecognizer(self.mapTap!)
        self.mapView.delegate = self
        self.tableView?.parallaxHeader.height = 250;
        self.tableView?.parallaxHeader.mode = MXParallaxHeaderMode.fill
        self.tableView?.parallaxHeader.view = self.mapView
        self.tableView?.backgroundColor = UIColor(red:0.22, green:0.27, blue:0.31, alpha:1.00).flatten()
        self.tableView?.separatorStyle = .none
        self.tableView?.allowsSelection = false
        
        if self.question.lat > 0 && self.question.long > 0 {
            self.goToLocation(CLLocation(latitude: self.question.lat, longitude: self.question.long))
        } else if self.position != nil {
            self.goToLocation(CLLocation(latitude: (self.position?.latitude)!, longitude: (self.position?.longitude)!))
        }
        
        form = Section("") { section in
            var header = HeaderFooterView<UIView>(.class)
            header.height = {0}
            header.onSetupView = { view, _ in

                
                
                
            }
            section.header = header
            }
            +++ Section()
            <<< ImageRow("image") { row in
                row.title = "Omslagsbild"
                row.sourceTypes = [.PhotoLibrary, .SavedPhotosAlbum, .Camera]
                row.clearAction = .yes(style: UIAlertActionStyle.destructive)
                    if (self.question.image != "") {
                        self.question.getImage(completion: { (image : UIImage) in
                            row.value = image
                        })
                    }
                /*
                let border = UIView(frame: CGRect(x: 16, y: Int(row.cell.frame.size.height) - 2, width: Int(self.screen.width - 32), height: 1))
                border.backgroundColor = .white
                row.cell.addSubview(border)
                 */
                
                
                }.cellUpdate { cell, row in
                    cell.accessoryView?.layer.cornerRadius = 17
                    cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                    cell.backgroundColor = .clear
                    cell.textLabel?.textColor = .white
        
            }
            <<< TextRow("title"){ row in
                row.value = self.question.title
                row.placeholder = "Fråga"
                row.placeholderColor = .white
                /*
                let border = UIView(frame: CGRect(x: 16, y: Int(row.cell.frame.size.height) - 2, width: Int(self.screen.width - 32), height: 2))
                border.backgroundColor = .white
                row.cell.addSubview(border)
                 */
                
                
                }.cellUpdate { cell, row in
                    cell.textField.textColor = .white
                    row.placeholderColor = UIColor.groupTableViewBackground
                    cell.tintColor = .white
                    cell.backgroundColor = UIColor(white: 1, alpha: 0.1)
            }
            <<< TextAreaRow("desc"){ row in
                row.placeholder = "Beskrivning (frivillig)"
                row.value = self.question.body

                }.cellUpdate { cell, row in
                    cell.placeholderLabel.textColor = UIColor.groupTableViewBackground
                    cell.textView.textColor = .white
                    cell.textView.backgroundColor = .clear
                    cell.backgroundColor = .clear
            }
        +++ Section()
            <<< SegmentedRow<String>("switchTypeRow") {
                $0.options = ["Välj", "Fri text"]
                if self.question.type == .media || self.question.type == .text  {
                    $0.value = "Fri text"
                } else {
                    $0.value = "Välj"
                }
                $0.baseCell.tintColor = .white
                $0.baseCell.backgroundColor = .clear
                }.onChange({ (row : SegmentedRow<String>) in
                    self.type = row.value!
                })
        +++ Section()
            <<< StepperRow("answerStepper") { stepper in
                print(self.question.type)
                stepper.title = "Svarsalternativ"
                stepper.value = Double(0)
                if self.question.type == .choose {
                    print(self.question.answers.count)
                    if self.question.answers.count > 0 {
                        stepper.value = Double(self.question.answers.count)
                    }
                }
                
                self.populateRow(slider: stepper)
                
                stepper.hidden = Condition.function(["switchTypeRow"], { form in
                    if let row = form.rowBy(tag: "switchTypeRow") {
                        if (row as! SegmentedRow).value == "Välj" {
                            return false
                        }
                    }
                    return true
                })
                
                }.cellUpdate({ (cell, row) in
                    cell.textLabel?.textColor = .white
                    cell.valueLabel.textColor = .white
                    cell.valueLabel.font = UIFont.boldSystemFont(ofSize: cell.valueLabel.font.pointSize)
                    cell.stepper.backgroundColor = .clear
                    cell.stepper.tintColor = .white
                    cell.backgroundColor = UIColor(white: 1, alpha: 0.1)
                    
                    
                }).onChange({ (stepper) in
                    stepper.cell.valueLabel.text = String(Int(Double(stepper.value!)))
                    let value : Int = Int(Float(stepper.value!))
                    let section : Section = self.form.allSections.last!
                    print(section.count)
                    // Add Row
                    if value >= section.count {
                        let count = value - section.count
                        for _ in 0...count {
                            self.addRow(id: "", text: "", isCorrect: false, index: section.count)
                        }
                    }
                        
                        // Remove Row
                    else if value < section.count - 1 {
                        let count = section.count - value - 2
                        print(count)
                        for _ in 0...count {
                            self.deleteRow()
                        }
                    }
                })
    }
    
    /*
     
     <<< SliderRow() { slider in
     slider.minimumValue = 1
     slider.maximumValue = 6
     slider.title = "Svarsalternativ"
     slider.baseCell.tintColor = .white
     slider.value = 0.0
     
     if self.question.type == .choose {
     if self.question.answers.count > 0 {
     slider.value = Float(self.question.answers.count)
     }
     }
     //self.populateRow(slider: slider)
     
     slider.hidden = Condition.function(["switchTypeRow"], { form in
     if let row = form.rowBy(tag: "switchTypeRow") {
     if (row as! SegmentedRow).value == "Välj" {
     return false
     }
     }
     return true
     })
     
     
     }.cellUpdate({ (cell, row) in
     cell.titleLabel.textColor = .white
     cell.valueLabel.textColor = .white
     cell.valueLabel.font = UIFont.boldSystemFont(ofSize: cell.valueLabel.font.pointSize)
     cell.slider.isContinuous = false
     cell.slider.backgroundColor = .clear
     cell.backgroundColor = .clear
     
     
     }).onChange({ (slider) in
     slider.value = Float(Int(Float(slider.value!)))
     let value : Int = Int(Float(slider.value!))
     let section : Section = self.form.allSections.last!
     
     // Add Row
     if value >= section.count {
     let count = value - section.count
     for _ in 0...count {
     self.addRow(id: "", text: "", isCorrect: false, index: section.count)
     }
     }
     
     // Remove Row
     else if value < section.count - 1 {
     let count = section.count - value - 1
     for _ in 0...count {
     self.deleteRow()
     }
     }
     })
     */
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.question.id.characters.count > 0 {
            self.goToLocation(CLLocation(latitude: CLLocationDegrees(self.question.lat), longitude: CLLocationDegrees(self.question.long)))
            self.addAnnotation(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(self.question.lat), longitude: CLLocationDegrees(self.question.long)))
        }
    }
    
    
    func populateRow(slider : StepperRow) -> Void {
        if self.question.id.characters.count > 0 {
            let ref = FIRDatabase
                .database()
                .reference()
                .child("questions")
                .child(self.question.id)
                .child("answers")
            
            ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
                let answer = answerModel(snap: snap)
                self.addRow(id: answer.id, text: answer.title, isCorrect: answer.isCorrect, index: Int(Double(slider.value!)))
                self.question.answers.append(answer)
                slider.value = slider.value! + Double(1)
                print("got question")
                print(slider.value!)
                slider.updateCell()
                slider.cell.valueLabel.text = String(Int(Double(slider.value!)))
            }
        }
        
    }
    
    func addRow(id : String, text : String, isCorrect : Bool, index : Int) -> Void {
        if let section = self.form.allSections.last {
            section.append(CustomQuestionRow("answer-" + String(index)) { row in
                /* syntax : text{}true{}id */
                row.value = text + "{}" + String(isCorrect) + "{}" + id
                row.placeholder = "Skriv här"
                row.setBackgroundColor = UIColor.clear
                row.hidden = Condition.function(["switchTypeRow"], { form in
                    if let row = form.rowBy(tag: "switchTypeRow") {
                        if (row as! SegmentedRow).value == "Välj" {
                            return false
                        }
                    }
                    return true
                })
            
                })
        }
    }
    
    func deleteRow() -> Void {
        var section = self.form.allSections.last!
        if section.count > 2 {
            _ = self.switches.popLast()
            section.remove(at: section.count - 1)
        }
    }
    
    
    // MARK : - Map View
    func goToLocation(_ location: CLLocation) {
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
    func addAnnotation(coordinate : CLLocationCoordinate2D) -> Void {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        self.mapView.addAnnotation(annotation)
    }
    
}

