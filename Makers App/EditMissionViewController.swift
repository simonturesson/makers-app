//
//  EditMissionViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-28.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import TransitionTreasury

class EditMissionViewController: FormViewController {
    
    let screen = UIScreen.main.bounds
    var user : userModel = userModel()
    var mission : missionModel = missionModel()
    
    weak var modalDelegate: ModalViewControllerDelegate?
    
    func dismiss() {
        modalDelegate?.modalViewControllerDismiss(callbackData: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func didTouchCancel(sender : AnyObject) -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTouchSave(sender : AnyObject) -> Void {
        var ref = FIRDatabase.database().reference().child("schools").child(self.user.schoolId).child("missions")
        let values = self.form.values()
        
        if self.mission.id.characters.count > 0 {
            ref = ref.child(self.mission.id)
        } else {
            ref = ref.childByAutoId()
        }
        let titleRow : TextRow = self.form.rowBy(tag: "title")!
        if (titleRow.value! as String).characters.count > 0 {
            values.forEach{(key: String, value: Any?) in
                if value != nil {
                    if key == "title" {
                        ref.child("title").setValue(value as! String)
                    }
                    
                    if key == "body" {
                        ref.child("body").setValue(value as! String)
                    }
                    
                    if key == "showUser" {
                        ref.child("showUserLocation").setValue(value as! Bool)
                    }
                    
                    if key == "mapType" {
                        ref.child("mapType").setValue(value as! String)
                    }
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.backgroundColor = UIColor(red:0.22, green:0.27, blue:0.31, alpha:1.00).flatten()
        self.tableView?.separatorStyle = .none
        self.tableView?.allowsSelection = false
        
        let close = UIButton(frame: CGRect(x: 16, y: 40, width: 100, height: 32))
        close.addTarget(self, action: #selector(EditMissionViewController.didTouchCancel(sender:)), for: UIControlEvents.touchUpInside)
        close.setTitle("Avbryt", for: UIControlState.normal)
        close.sizeToFit()
        
        let save = UIButton(frame: CGRect(x: self.screen.width - 116, y: 40, width: 100, height: 32))
        save.addTarget(self, action: #selector(EditMissionViewController.didTouchSave(sender:)), for: UIControlEvents.touchUpInside)
        save.setTitle("Spara", for: UIControlState.normal)
        save.backgroundColor = UIColor.green.flatten()
        save.layer.cornerRadius = 16
        
        view.addSubview(close)
        view.addSubview(save)
        
        form = Section("") { section in
            var header = HeaderFooterView<UIView>(.class)
            header.height = {64}
            header.onSetupView = { view, _ in
                
            }
            section.header = header
            }
            +++ Section("")
            <<< TextRow("title"){ row in
                row.placeholder = "Titel"
                row.placeholderColor = .white
                row.value = self.mission.title
                
               let border = UIView(frame: CGRect(x: 16, y: Int(row.cell.frame.size.height) - 2, width: Int(self.screen.width - 32), height: 1))
                border.backgroundColor = .white
                row.cell.addSubview(border)
                
                }.cellUpdate { cell, row in
                    cell.textField.textColor = .white
                    row.placeholderColor = UIColor.groupTableViewBackground
                    cell.tintColor = .white
                    cell.backgroundColor = .clear
            }
            <<< TextAreaRow("body"){ row in
                row.placeholder = "Beskrivning (frivillig)"
                if self.mission.body.characters.count > 0 {
                    row.value = self.mission.body
                }
 
                
                }.cellUpdate { cell, row in
                    cell.placeholderLabel.textColor = UIColor.groupTableViewBackground
                    cell.textView.textColor = .white
                    cell.textView.backgroundColor = .clear
                    cell.backgroundColor = .clear
        }
            +++ Section("Inställningar") { section in
                
            }
            <<< SwitchRow("showUser") { row in
                row.title = "Visa användarens position på kartan"
                row.value = self.mission.showUserLocation
                
                
                }.cellUpdate { cell, row in
                    cell.backgroundColor = .clear
                    cell.isSelected = true
                    cell.textLabel?.textColor = .white
            }
            <<< SegmentedRow<String>("mapType") {
                $0.options = ["Standard", "Hybrid", "Satelit"]
                $0.value = self.mission.mapType
                $0.baseCell.tintColor = .white
                $0.baseCell.backgroundColor = .clear
            }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}
