//
//  Global.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-25.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import FirebaseAuth
import SVProgressHUD
import ChameleonFramework

class PassThroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.alpha > 0 && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}

extension UIViewController {
    func shortTimeAgoSinceDate(date: NSDate) -> String {
        return ""
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }

    
    
    func background() {
        DispatchQueue.global().async {
          
            
            DispatchQueue.main.async {
              
            }
        }
    }
    
    func setGradient(view : UIView) -> Void {
        let color : [UIColor] = [
            Settings.sharedInstance.mainBgColorFrom,
            Settings.sharedInstance.mainBgColorTo
            
        ]
        var frame : CGRect?
        
        if view.frame.height >= view.frame.width {
         frame = CGRect(x: 0, y: 0, width: Int(view.frame.height), height: Int(view.frame.height))
        } else {
        frame = CGRect(x: 0, y: 0, width: Int(view.frame.width), height: Int(view.frame.width))
        }
        view.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame!, andColors: color)
        print("setting bg")
    }
    
    // MARK : - User from Firebase
    func returnUserRef(completion: @escaping (userModel) -> Void) -> Void {
        var isRetured : Bool = false
        if let user = FIRAuth.auth()?.currentUser {
            let ref = FIRDatabase.database().reference().child("users")
            ref.queryOrderedByKey().observe(.childAdded, with: { (snap : FIRDataSnapshot) in
                let data : NSDictionary = snap.value as! NSDictionary
                if data["uid"] as? String == user.uid {
                    if !isRetured {
                        isRetured = true
                        completion(userModel(snap: snap))
                    }
                }
            })
        }
    }
    
    // MARK : - Navbar style
    func setNavbarStyle(navigationBar : UINavigationBar) {
        let img = UIImage()
        automaticallyAdjustsScrollViewInsets = true
        navigationBar.isTranslucent = false
        navigationBar.shadowImage = img
        navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationBar.barTintColor = Settings.sharedInstance.navBarTintColor
        navigationBar.tintColor = Settings.sharedInstance.navBarTint
    }
    
    func showSpinner() -> Void {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(UIColor(white: 1, alpha: 0.4))
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setRingRadius(CGFloat(28))
        SVProgressHUD.setCornerRadius(CGFloat(50))
        SVProgressHUD.show()
    }
    
    func hideSpinner() -> Void {
        SVProgressHUD.dismiss()
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
