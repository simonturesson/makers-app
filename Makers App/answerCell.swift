//
//  answerCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-22.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import M13Checkbox

class answerCell: UITableViewCell {

    @IBOutlet weak var checkbox: M13Checkbox!
    @IBOutlet weak var label: UILabel!
}
