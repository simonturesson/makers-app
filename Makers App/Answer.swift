//
//  answer.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-23.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

struct answerModel {
    
    init(snap : FIRDataSnapshot) {
        let data : NSDictionary = snap.value as! NSDictionary
        self.id = snap.key
        
        if data["title"] != nil  {
            self.title =  data["title"] as! String
        }
        
        if data["isCorrect"] != nil  {
            self.isCorrect = data["isCorrect"] as! Bool
        }
    }
    
    init() {
        
    }
    
    var id : String = ""
    var title : String = ""
    var isCorrect : Bool = false
    var isChanged : Bool = false
    var isDeleted : Bool = false
    var created : String = ""
    var updated : String = ""
}
