//
//  ViewController.swift
//  Makers App
//
//  Created by Simon Turesson on 2016-09-02.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseMessaging
import CZPicker
import DateTools
import DZNEmptyDataSet
import DynamicButton
import TransitionTreasury
import TransitionAnimation


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, CZPickerViewDelegate, CZPickerViewDataSource, MenuTransitionManagerDelegate, ModalTransitionDelegate {
    
    var tr_presentTransition: TRViewControllerTransitionDelegate?
    
    // MARK : - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentUser: UIBarButtonItem!
    @IBOutlet weak var timeLabel: SpringLabel!
    @IBOutlet weak var distanceLabel: SpringLabel!
    @IBOutlet weak var correctionRatioLabel: SpringLabel!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIBarButtonItem!
    
    // MARK : - Actions
    @IBAction func didTouchAdd(_ sender: Any) {
        self.editMission(mission: missionModel())
    }
    
    
    @IBAction func didTouchProfileBtn(_ sender: AnyObject) {
        
    }
    
    @IBAction func didTouchInbox(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "inboxSegue", sender: self)
    }
    
    @IBAction func didTouchEdit(_ sender: AnyObject) {
        self.editMission(mission: missionModel())
    }
    
    @IBAction func logOut(_ sender: AnyObject) {
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            self.performSegue(withIdentifier: "logoutSegue", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: \(signOutError.localizedDescription)")
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    let menuTransitionManager = MenuTransitionManager()
    var user : userModel = userModel()
    var users : [userModel] = []
    let sections : [String] = ["Nya Uppgrag", "Slutförda uppdrag"]
    var completed : [completedModel] = []
    let screenSize: CGRect = UIScreen.main.bounds
    var picker : CZPickerView?
    var pickedMission : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setGradient(view: self.view)
        self.logoutBtn.isEnabled = true
        self.logoutBtn.tintColor = .white
        self.currentUser.title = ""
        self.currentUser.isEnabled = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = Settings.sharedInstance.tableCellBottomBorderColor
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.rowHeight = 50.0
        tableView.tableFooterView = UIView()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        // MARK : - Picker
        self.picker = CZPickerView(headerTitle: "Skicka uppdraget till elever", cancelButtonTitle: "Avbryt", confirmButtonTitle: "Skicka")
        self.picker?.delegate = self
        self.picker?.dataSource = self
        self.picker?.headerBackgroundColor = Settings.sharedInstance.mainBgColorTo
        self.picker?.cancelButtonNormalColor = UIColor.gray.flatten()
        self.picker?.confirmButtonNormalColor = UIColor.white
        self.picker?.checkmarkColor = Settings.sharedInstance.mainBgColorTo
        self.picker?.headerTitleColor = UIColor.white
        self.picker?.allowMultipleSelection = true
        
        // MARK : - Get user
        self.returnUserRef { (user : userModel) in
            self.currentUser.title = "Välkommen " + user.firstName.capitalizingFirstLetter()
            self.correctionRatioLabel.text = (user.successRate != "") ? user.successRate : "-"
            self.distanceLabel.text = (user.totalDistance != "") ? user.totalDistance : "-"
            self.timeLabel.text = (user.totalTime != "") ? user.totalTime : "-"
            self.getUsers()
            self.user = user
            if self.user.type == .admin {
                self.createBtn.isHidden = false
                self.createBtn.isEnabled = true
            }
            
            FIRDatabase.database().reference().child("schools").child(user.schoolId).queryOrderedByKey().observe(.childAdded, with: { (snap : FIRDataSnapshot) in
                self.user.school = schoolModel(snap: snap)
                self.findCompleted(ref: FIRDatabase.database().reference().child("completed"))
                self.findAssigned()
            })
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            navigationItem.title = "Map run"
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.navigationBar.alpha = 1
            for view in self.view.subviews {
                view.alpha = 1
            }
        if self.user.id.characters.count > 0 {
            self.updateStats()
        }
    }
    
    func findAssigned() {
        let ref = FIRDatabase.database().reference()
        

        if self.user.type == .admin {
            // Admins
            ref.child("schools").child(self.user.schoolId).child("missions").observe(FIRDataEventType.childAdded, with: { (snap : FIRDataSnapshot) in
                if snap.exists() {
                    if !self.user.school.missions.contains(where: { $0.id == snap.key }) {
                        self.user.school.missions.insert(missionModel(snap: snap, schoolId: self.user.school.id), at: 0)
                        self.tableView.reloadData()
                    }
                }
            })
            
            ref.child("schools").child(self.user.schoolId).child("missions").observe(FIRDataEventType.childChanged, with: { (snap : FIRDataSnapshot) in
                if snap.exists() {
                    print(snap)
                    if self.user.school.missions.count > 0 {
                    for index in 0...(self.user.school.missions.count - 1) {
                        if self.user.school.missions[index].id == snap.key {
                            self.user.school.missions[index] = missionModel(snap: snap, schoolId: self.user.school.id)
                            self.tableView.reloadData()
                        }
                    }
                }
                }
            })
        
            ref.child("schools").child(self.user.schoolId).child("missions").observe(FIRDataEventType.childAdded, with: { (snap : FIRDataSnapshot) in
                if snap.exists() {
                    if !self.user.school.missions.contains(where: { $0.id == snap.key }) {
                        self.user.school.missions.insert(missionModel(snap: snap, schoolId: self.user.school.id), at: 0)
                        self.tableView.reloadData()
                    }
                }
            })
            
        } else {
        
            // Users
            ref.child("assignments").observe(.childAdded) { (snap : FIRDataSnapshot) in
                let data = snap.value as! NSDictionary
                if (data["user"] as! String) == self.user.id {
                    if (data["isCompleted"] as! Bool) == false {
                        let missionId = data["missionId"] as! String
                        let created = data["created"] as! String
                        let assignment = snap.key
                        print(assignment)
                        
                        ref.child("schools").child(self.user.schoolId).child("missions").child(missionId).observeSingleEvent(of: FIRDataEventType.value, with: { (snap: FIRDataSnapshot) in
                            if snap.exists() {
                                if !self.user.school.missions.contains(where: { $0.id == snap.key }) {
                                    var model = missionModel(snap: snap, schoolId: self.user.school.id)
                                    model.created = created
                                    model.assignmentId = assignment
                                    self.user.school.missions.insert(model, at: 0)
                                    self.tableView.reloadData()
                                }
                            }
                        })
                        
                        ref.child("schools").child(self.user.schoolId).child("missions").child(missionId).observeSingleEvent(of: FIRDataEventType.childChanged, with: { (snap: FIRDataSnapshot) in
                            print(snap)
                            for index in 0...(self.user.school.missions.count - 1) {
                                if self.user.school.missions[index].id == missionId {
                                    switch snap.key {
                                        case "title":
                                            self.user.school.missions[index].title = snap.value! as! String
                                            break
                                        case "body":
                                            self.user.school.missions[index].body = snap.value! as! String
                                        break
                                    default:
                                        break
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        })
                    }
                }
            }

            
        }
    }
    
    func findCompleted(ref : FIRDatabaseReference) -> Void {
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            if !self.completed.contains(where: { $0.id == snap.key }) {
                let data = snap.value as! NSDictionary
                let users : NSArray = data["users"] as! NSArray
                users.forEach({ (id : Any) in
                    if id as! String == self.user.id {
                        self.completed.insert(completedModel(snap: snap), at: 0)
                        self.tableView.reloadData()
                    }
                })
            }
        }
        
        ref.queryOrderedByKey().observe(FIRDataEventType.childRemoved, with: { (snap : FIRDataSnapshot) in
           
        })
    }
    
    // MARK : - OLD (not using)
    func findMissions(ref : FIRDatabaseReference) -> Void {
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            if !self.user.school.missions.contains(where: { $0.id == snap.key }) {
                self.user.school.missions.insert(missionModel(snap: snap, schoolId: self.user.school.id), at: 0)
                self.tableView.reloadData()
            }
        }
        
        ref.queryOrderedByKey().observe(.childChanged) { (snap : FIRDataSnapshot) in
            
            for (index, item) in self.user.school.missions.enumerated() {
                if item.id == snap.key {
                    self.user.school.missions[index] = missionModel(snap: snap, schoolId: self.user.school.id)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK : - Using?
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = self.user.school.missions.count - 1
        let val = (CGFloat(index) / CGFloat(itemCount)) * 0.7
        return UIColor(red:1.00 , green:val, blue:0.50, alpha: 0.1)
    }
    
    // MARK : - Edit Mission
    func editMission(mission : missionModel) {
        let viewControllerStoryboardId = "editStoryboard"
        let storyboardName = "Main"
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerStoryboardId) as! EditMissionViewController
        controller.user = self.user
        controller.mission = mission
        controller.modalDelegate = self // Don't forget to set modalDelegate
        tr_presentViewController(controller, method: TRPresentTransitionMethod.twitter, completion: {
            print("Present finished.")
        })
    }
    
    // MARK : - Empty data set
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Kan inte hitta några uppdrag till dig!"
        
        let attrs = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 28), NSForegroundColorAttributeName: UIColor.white]
        
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image = #imageLiteral(resourceName: "island")
        return image.imageScaled(to: CGSize(width: 8*70, height: 5*70))
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Kontrollera din internet uppkoppling"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body), NSForegroundColorAttributeName: UIColor.white]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    // MARK : - Table view
    func returnMissionId(indexPath : IndexPath) -> String {
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            if indexPath.section == 0 {
                return self.user.school.missions[indexPath.row].id
            } else {
                return self.completed[indexPath.row].missionId
            }
        }
        
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            if self.user.school.missions.count > 0 {
                return self.user.school.missions[indexPath.row].id
            } else {
                return self.completed[indexPath.row].missionId
            }
        }
        
        return ""
    }
    
    func returnMissionTitle(indexPath : IndexPath) -> String {
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            if indexPath.section == 0 {
                return self.user.school.missions[indexPath.row].title
            } else {
                return self.completed[indexPath.row].title
            }
        }
        
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            if self.user.school.missions.count > 0 {
                return self.user.school.missions[indexPath.row].title
            } else {
                return self.completed[indexPath.row].title
            }
        }
        
        return ""
    }
    
    func returnMission(indexPath : IndexPath) -> Any? {
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            if indexPath.section == 0 {
                return self.user.school.missions[indexPath.row]
            } else {
                return self.completed[indexPath.row]
            }
        }
        
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            if self.user.school.missions.count > 0 {
                return self.user.school.missions[indexPath.row]
            } else {
                return self.completed[indexPath.row]
            }
        }
        
        return nil
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            return 2
        }
    
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            return 1
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            if section == 0 {
                return self.user.school.missions.count
            } else {
                return self.completed.count
            }
        }
        
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            if self.user.school.missions.count > 0 {
                return self.user.school.missions.count
            } else {
                return self.completed.count
            }
        }
        
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: "Ta bort") { (UITableViewRowAction, indexPath) in
            self.tableView.setEditing(false, animated: true)
            let model = self.returnMission(indexPath: indexPath)
            if model is missionModel {
                let mission = model as! missionModel
                self.user.school.missions.remove(at: indexPath.row)
                FIRDatabase.database().reference().child("schools").child(self.user.school.id).child("missions").child(mission.id).removeValue()
            } else {
                self.completed.remove(at: indexPath.row)
                let comp = model as! completedModel
                FIRDatabase.database().reference().child("completed").child(comp.id).removeValue()
            }
            self.tableView.reloadData()
            self.updateStats()
        }
        
        let move = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Redigera") { (UITableViewRowAction, indexPath) in
            self.tableView.setEditing(false, animated: true)
            for mission in self.user.school.missions {
                if mission.id == self.returnMissionId(indexPath: indexPath) {
                    self.editMission(mission: mission)
                }
            }
        }
        
        let send = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Skicka till elever") { (UITableViewRowAction, indexPath) in
            self.tableView.setEditing(false, animated: true)
            self.pickedMission = self.returnMissionId(indexPath: indexPath)
            self.picker?.show()
        }
        
        send.backgroundColor = UIColor.green.flatten()
        move.backgroundColor = UIColor.blue.flatten()
        delete.backgroundColor = UIColor.red.flatten()
        
        if self.returnMission(indexPath: indexPath) is missionModel {
            return [send, move, delete]
        }
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.detailTextLabel?.text = "test"
        /*
        if  self.returnMissionTimeAgo(indexPath: indexPath) != "" {
            let date = NSDate(string: self.returnMissionTimeAgo(indexPath: indexPath), formatString: "yyyy-MM-dd HH:mm:ss Z")
            //cell.detailTextLabel?.text = shortTimeAgoSinceDate(date: date!)
        }
 */
        cell.textLabel?.text = self.returnMissionTitle(indexPath: indexPath)
        cell.backgroundColor = UIColor(white: 1, alpha: 0.1)
        cell.separatorInset = .zero
        cell.layoutMargins = .zero
        if self.returnMission(indexPath: indexPath) is completedModel {
            print("is completed")
            cell.accessoryType = .none
            cell.selectionStyle = .none
        } else {
            print("is not completed")
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .default
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.returnMission(indexPath: indexPath) is missionModel {
            self.performSegue(withIdentifier: "missionSegue", sender: self)
        }
    }
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        navigationItem.title = ""
        
        if segue.identifier == "missionSegue" {
            let VC = segue.destination as! MapViewController
            let index : IndexPath = self.tableView.indexPathForSelectedRow! as IndexPath
            VC.user = self.user
            VC.mission = self.user.school.missions[index.row]
            menuTransitionManager.bottomOffset = 0
            menuTransitionManager.delegate = self
            self.tableView.deselectRow(at: index, animated: true)

        }
        
        
        if segue.identifier == "completedSegue" {
            let VC = segue.destination as! MissionStatsViewController
            let index : IndexPath = self.tableView.indexPathForSelectedRow! as IndexPath
            VC.completed = self.completed[index.row]
            self.tableView.deselectRow(at: index, animated: true)

        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 1)
        if indexPath.row == lastRowIndex - 1 {
            self.updateStats()
        }
        self.tableView.alpha = 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.completed.count > 0 && self.user.school.missions.count > 0 {
            if section == 0 {
                return self.sections[0]
            } else {
                return self.sections[1]
            }
        }
        
        
        if self.completed.count > 0 || self.user.school.missions.count > 0 {
            if self.user.school.missions.count > 0 {
                return self.sections[0]
            } else {
                return self.sections[1]
            }
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = UIColor.white
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
       if self.user.type == .admin {
            return true
        }
        return false
    }
    
    // MARK : - CZPickerView
    func getUsers() -> Void {
        let ref = FIRDatabase.database().reference().child("users")
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            let model = userModel(snap: snap)
            //if model.id != self.user.id {
                if model.schoolId == self.user.schoolId {
                    self.users.append(model)
                    self.picker?.reloadData()
                }
            //}
        }
        
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [Any]!) {
        for row in rows as! [Int] {
            let ref = FIRDatabase.database().reference().child("assignments").childByAutoId()
            ref.setValue([
                "missionId" : self.pickedMission,
                "user" : self.users[row].id,
                "isCompleted" : false,
                "created" : String(describing: NSDate()),
                "updated": String(describing: NSDate())
                ])
        }
        pickerView.reloadData()
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return self.users[row].firstName.capitalizingFirstLetter() + " " + self.users[row].lastName.capitalizingFirstLetter()
    }
    
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return self.users.count
    }
    
    // MARK : - Update statisics
    var totalTime = NSDate()
    var totalDistance : Double = 0
    var correctCount : Int = 0
    var answersCount : Int = 0

    func updateStats() -> Void {
        self.totalTime = NSDate()
        self.totalDistance = 0
        self.correctCount = 0
        self.answersCount = 0
        
        if self.completed.count > 0 {
            for item in self.completed {
                
                // MARK : - Time
                /*
                let timeArr = item.time.components(separatedBy: " ")
                for curr in timeArr {
                    var dateString = ""
                    switch curr.characters.last! {
                    case "m":
                        //dateString = String(describing: self.totalTime.addingMinutes(Int(curr.substring(to: curr.index(before: curr.endIndex)))!)!)
                        break
                    case "s":
                        //dateString = String(describing: self.totalTime.addingSeconds(Int(curr.substring(to: curr.index(before: curr.endIndex)))!)!)
                        break
                    default: break
                    }
                    
                   // if NSDate(string: dateString, formatString: "yyyy-MM-dd HH:mm:ss Z") != nil {
                   //     self.totalTime = NSDate(string: dateString, formatString: "yyyy-MM-dd HH:mm:ss Z")
                   // }
                    
                }
 */
                
                // MARK : - Success rate
                if item.questions.count > 0 {
                    for quest in item.questions {
                        if quest.answers.count > 0 {
                            for answer in quest.answers {
                                self.answersCount = self.answersCount + 1
                                if answer.isCorrect {
                                    self.correctCount = self.correctCount + 1
                                }
                            }
                        }
                    }
                    
                    if self.answersCount != 0 {
                        self.correctionRatioLabel.text = String(Int((Float(self.correctCount)/Float(self.answersCount)) * 100)) + "%"
                    } else {
                        self.correctionRatioLabel.text = "0%"
                    }
                }
                
                
                
                // MARK : - Distance
                self.totalDistance = Double(item.distance)!
                //self.timeLabel.text = self.totalTime.shortTimeAgoSinceNow()
                self.distanceLabel.text = formatDistance(meters: Float(self.totalDistance))
            }
        }


        //self.timeLabel.text = self.totalTime.shortTimeAgoSinceNow()
        self.distanceLabel.text = formatDistance(meters: Float(self.totalDistance))
        
        let ref = FIRDatabase.database().reference().child("users").child(self.user.id)
        ref.child("successRate").setValue(self.correctionRatioLabel.text)
        ref.child("totalTime").setValue(self.timeLabel.text)
        ref.child("totalDistance").setValue(self.distanceLabel.text)
    }
    
}
