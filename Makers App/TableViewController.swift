//
//  TableViewController.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit
import Firebase

class TableViewController: UITableViewController, UISplitViewControllerDelegate {

    var detailViewController: MasterViewController? = nil
    let labels : [String] = ["Nya", "Lästa", "Rättade"]
    var array : [[completedModel]] = [[], [],[]]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func didTouchClose(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.splitViewController?.delegate = self
        self.splitViewController?.preferredDisplayMode = .allVisible
        self.clearsSelectionOnViewWillAppear = false
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        self.setGradient(view: self.view)
        self.loadData()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl!) // not required when using UITableViewController
    }
    
    func refresh(sender:AnyObject) {
        self.array = [[], [],[]]
        self.loadData()
        self.refreshControl?.endRefreshing()
    }
    
    func loadData() {
        let ref = FIRDatabase.database().reference().child("completed")
        ref.queryOrderedByKey().observe(.childAdded) { (snap : FIRDataSnapshot) in
            let model = completedModel(snap: snap)
            
            if !model.isRead && model.state == .unknown {
                self.array[0].insert(model, at: 0)
            } else if model.state == .unknown {
                self.array[1].insert(model, at: 0)
            } else {
                self.array[2].insert(model, at: 0)
            }
            
            
            self.tableView.reloadData()
        }
    }


    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return labels.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.labels[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.array[section].count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let data = self.array[indexPath.section][indexPath.row]
        cell.backgroundColor = UIColor.clear
        cell.detailTextLabel?.text = data.title
        if  data.created.characters.count > 10 {
            print(data.created)
            (cell.viewWithTag(10) as! UILabel).text = NSDate(string: data.created, formatString: "yyyy-MM-dd HH:mm:ss Z").shortTimeAgoSinceNow()
        } else {
            (cell.viewWithTag(10) as! UILabel).text = ""
        }
        
        for (index, user) in data.users.enumerated() {
            let ref = FIRDatabase.database().reference().child("users").child(user)
            ref.observe(.childAdded, with: { (snap : FIRDataSnapshot) in
                if snap.key == "firstName" {
                    var extra = ", "
                    
                    if index == 0 {
                        cell.textLabel?.text = ""
                    }
                    
                    if data.users.count == 1 {
                        extra = ""
                    } else if data.users.count == 2 {
                        extra = " & "
                    } else {
                        if index == (data.users.endIndex - 1) {
                            extra = " & "
                        }
                    }
                    
                    let name = snap.value as! String
                    cell.textLabel?.text = (cell.textLabel?.text)! + name.capitalizingFirstLetter() + extra
                }
            })
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let VC = segue.destination as! MasterViewController
            let index : NSIndexPath = self.tableView.indexPathForSelectedRow! as NSIndexPath
            VC.completed = self.array[index.section][index.row]
        }
    }


}
