//
//  Question.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-20.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

class questionModel {
    init(snap : FIRDataSnapshot) {
        let data : NSDictionary = snap.value as! NSDictionary
        self.id = snap.key
        
        if data["missionId"] != nil  {
            self.mission =  data["missionId"] as! String
        }
        
        if data["title"] != nil  {
            self.title =  data["title"] as! String
        }
        if data["body"] != nil  {
            self.body =  data["body"] as! String
        }
        if data["type"] != nil  {
            self.type = AnswerType(rawValue: data["type"] as! String )
        }
        
        if data["chooseType"] != nil  {
            self.chooseType = AnswerChooseType(rawValue: data["chooseType"] as! String )
        }
        
        if data["lat"] != nil  {
            self.lat = data["lat"] as! Double
        }

        if data["long"] != nil  {
            self.long = data["long"] as! Double
        }
        
        if  data["image"] != nil {
            self.image = data["image"] as! String
        }
    }
    
    init() {
        
    }
    
    var id : String = ""
    var title : String = ""
    var body : String = ""
    var type : AnswerType = .text
    var chooseType : AnswerChooseType = .radio
    var lat : Double = Double(0)
    var long : Double = Double(0)
    var image : String = ""
    var created : String = ""
    var updated : String = ""
    var mission : String = ""
    var answers : [answerModel] = []
    var isLocked : Bool = true
    var number : Int = 0
    
    func getImage( completion: @escaping (UIImage) -> Void)  {
        FIRStorage.storage().reference(forURL: self.image).data(withMaxSize: 10*1024*1024, completion: { (data, error) in
            if (data != nil) {
                completion(UIImage(data: data!)!)
            }
        })
    }
}

