//
//  Settings.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-29.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class Settings: NSObject {
    
    static let sharedInstance : Settings = {
        let instance = Settings(array: [])
        return instance
    }()
    
    //MARK: Local Variable
    
    var emptyStringArray : [String]
    
    //MARK: Init
    
    init( array : [String]) {
        emptyStringArray = array
    }
    
    // MARK : - Color Scheme
    
    //BG GRADIENT
    var mainBgColorFrom : UIColor = UIColor(red:0.25, green:0.37, blue:0.44, alpha:1.00)
    var mainBgColorTo : UIColor = UIColor(red:0.16, green:0.29, blue:0.37, alpha:1.00)
    
    //NavBar color
    var navBarTintColor :  UIColor = UIColor(red: 0.8275, green: 0.2118, blue: 0.2392, alpha: 1).flatten()
    var navBarTint : UIColor = UIColor(red:0.99, green:0.99, blue:1.00, alpha:1.00)
    
    
    var mainColor : UIColor = UIColor.white
    var secondaryColor : UIColor = UIColor(red:0.96, green:0.97, blue:0.98, alpha:1.00)
    
    var tableCellBottomBorderColor : UIColor = UIColor(white: 1, alpha: 0.3)
    
    //select colors
    var selectedQuestion : UIColor = UIColor(red:0.44, green:0.57, blue:0.64, alpha:1.00)

}
