//
//  completedAnswers.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-09-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import Firebase
import Foundation

struct completedAnswersModel {
    init(snap : FIRDataSnapshot) {
        let data : NSDictionary = snap.value as! NSDictionary
        self.id = snap.key
        
        if data["isCorrect"] != nil  {
            self.isCorrect =  data["isCorrect"] as! Bool
        }
        if data["photoUrl"] != nil  {
            self.photoUrl =  data["photoUrl"] as! String
        }
        if data["body"] != nil  {
            self.body = data["body"] as! String
        }
    }
    
    init() {
        
    }
    
    
    var id : String = ""
    var isCorrect : Bool = false
    var photoUrl : String = ""
    var body : String = ""
    var answers : [String] = []
}
