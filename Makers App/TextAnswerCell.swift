//
//  TextAnswerCell.swift
//  Makers App
//
//  Created by Per Sonberg on 2016-11-30.
//  Copyright © 2016 persimon. All rights reserved.
//

import UIKit

class TextAnswerCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var textArea: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textArea.delegate = self
        if textArea.text == "" || textArea.text == "Skriv här" {
            textArea.text = "Skriv här"
            textArea.textColor = UIColor.lightGray
        } else {
            textArea.textColor = UIColor.white

        }
        //textArea.selectedTextRange = textArea.textRange(from: textArea.beginningOfDocument, to: textArea.beginningOfDocument)
    }
    
    func updateStyle() -> Void {
        if textArea.text == "" || textArea.text == "Skriv här" {
            textArea.text = "Skriv här"
            textArea.textColor = UIColor.lightGray
        } else {
            textArea.textColor = UIColor.white
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Skriv här" {
            print("resetting")
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Skriv här"
            textView.textColor = UIColor.lightGray
        }
    }

}
